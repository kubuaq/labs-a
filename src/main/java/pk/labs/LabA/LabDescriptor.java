package pk.labs.LabA;

public class LabDescriptor {

    // region P1

    public static String displayImplClassName = "pk.labs.LabA.display.DisplayBean";
    public static String controlPanelImplClassName = "pk.labs.LabA.controlpanel.ControlPanelBean";

    public static String mainComponentSpecClassName = "pk.labs.LabA.Contracts.Ekspres";
    public static String mainComponentImplClassName = "pk.labs.LabA.main.EkspresBean";
    // endregion

    // region P2
    public static String mainComponentBeanName = "main";
    public static String mainFrameBeanName = "app";
    // endregion

    // region P3
    public static String sillyFrameBeanName = "sillyframe";
    // endregion
}
